<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <title>Grid Simulation</title>

    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/reveal.css">
    <link rel="stylesheet" href="css/theme/white.css">
    <link rel="shortcut icon" href="img/favicon.ico">

    <!-- Theme used for syntax highlighting of code -->
    <link rel="stylesheet" href="lib/css/monokai.css">

    <!-- Printing and PDF exports -->
    <script>
        var link = document.createElement( 'link' );
        link.rel = 'stylesheet';
        link.type = 'text/css';
        link.href = window.location.search.match( /print-pdf/gi ) ? 'css/print/pdf.css' : 'css/print/paper.css';
        document.getElementsByTagName( 'head' )[0].appendChild( link );
    </script>
</head>
<body>
<div class="reveal">
    <div class="slides">
        <section data-background="linear-gradient(#c2ccd4, #ffffff)">
            <img src="img/Splash.svg" style="border-width: 0; background-color: transparent; box-shadow: initial;">

            <h2>N7 Grid Simulation using Smart Meter Data</h2>

            <h4>Derrick Oswald</h4>
            <audio controls>
                <source src="media/splash.mp3" type="audio/mp3">
            </audio>
            <aside class="notes">
                N7 Grid Simulation using Smart Meter Data
                by
                Derrick Oswald
                The electric grid, composed of nodes such as switches and service points,
                and edges such as transformers and cables is partitioned into levels,
                where level N7 corresponds to the low voltage grid,
                which is the usual level at domestic customer meters.
                Simulation involves determining the voltages, currents
                and powers at the nodes and edges over time.
            </aside>
        </section>

        <section data-background="linear-gradient(#c2ccd4, #ffffff)">
            <h3>Motivation:</h3>
            <div style="text-align: left;">• smart meter data:
                <div style="font-size: smaller; text-indent: 1em;">• 15 minute measurements</div>
                <div style="font-size: smaller; text-indent: 1em;">• 50 houses per transformer service area</div>
                <div style="font-size: smaller; text-indent: 1em;">• 5000 service areas</div>
                <div style="font-size: smaller; text-indent: 1em;">• 96×50×5000 = 24 million/day</div>
            </div>
            <div>• low voltage network: 1-10GB CIM file (uncompressed)</div>
            <h3>Use-cases:</h3>
            <div style="text-align: left;">
                <div style="font-size: smaller">• which transformers can service these customers</div>
                <div style="font-size: smaller">• what if every tenth house gets a Tesla</div>
                <div style="font-size: smaller">• can the infrastructure support another apartment building</div>
            </div>
            <audio controls>
                <source src="media/motivation.mp3" type="audio/mp3">
            </audio>
            <aside class="notes">
                By way of motivation, smart meter data volume can be millions of readings per day,
                and the size of the low voltage distribution grid – because of the large number of nodes and edges –
                can be gigabytes.
                This requires “Big Data” techniques – large clusters of compute and database servers
                to answer use-case questions pertinent to electric distribution engineers.
            </aside>
        </section>
        <section data-background="linear-gradient(#c2ccd4, #ffffff)">
            <h3>Architecture</h3>
            <img src="img/Architecture.svg" style="border-width: 0; background-color: transparent; box-shadow: initial;">
            <audio controls>
                <source src="media/architecture.mp3" type="audio/mp3">
            </audio>
            <aside class="notes">
                The architecture we've settled on has three configurations –
                the leftmost two channels are for batch & interactive compute, and the rightmost channel is for query.
                Spark is needed only for the two compute channels
                – so it can be shut down to reduce costs when only the query channel is needed.
                The Cassandra database, on the other hand, is needed for both compute and query.
                The batch & interactive compute components are open source.
            </aside>
        </section>
        <section data-background="linear-gradient(#c2ccd4, #ffffff)">
            <h3>Processes</h3>
            <div style="text-align: left;">
                <div style="font-size: smaller">• Ingest – head-end-system smart meter data into Cassandra</div>
                <div style="font-size: smaller">• CIM Generation – generation of network snapshot CIM files</div>
                <div style="font-size: smaller">• Topological Analysis – node/breaker to bus/branch & islands</div>
                <div style="font-size: smaller">• Export – export topological islands (transformer service areas)</div>
                <div style="font-size: smaller">• Baseline simulation – generate time series for everything</div>
                <div style="font-size: smaller">• Project/use-case simulation</div>
            </div>
            <audio controls>
                <source src="media/processes.mp3" type="audio/mp3">
            </audio>
            <aside class="notes">
                I'll cover the end-to-end process workflow, including:
                Ingest – which moves head-end-system smart meter data into Cassandra
                CIM Generation – that creates a network snapshot, in the form of CIM files (Common Information Model)
                Topological Analysis – to convert from a node/breaker to a bus/branch model,
                and also identifies topological islands (or transformer service areas)
                Export – to partition the large CIM files into these topological islands
                for downstream use
                Baseline simulation – to run load-flow calculations, using the available meter data,
                which generates time series for voltages, currents and powers for all network components
                in the valid time period of the meter data
                and
                Project or Use-Case simulations for a number of scenarios of interest
            </aside>
        </section>
        <section data-background="linear-gradient(#c2ccd4, #ffffff)">
            <h3>Ingest</h3>
            <img src="img/Ingest.svg" style="border-width: 0; background-color: transparent; box-shadow: initial;">
            <audio controls>
                <source src="media/ingest.mp3" type="audio/mp3">
            </audio>
            <aside class="notes">
                Smart meter data is collected from customer meters through the concentrators into the head-end-system.
                The head-end system generates (near) CSV files, one row per day
                – where the difference is at daylight savings time changes,
                where twice a year there are four more or less readings in a day.
                The system normally provides apparent power – because it's based on billing system requirements
                – but sometimes active and reactive power are available.
                Currently, readings are captured at 15 minute intervals approximately 300MB per year per substation.
                The ingest phase demultiplexes the daily reading records,
                and joins them to a CIM EnergyConsumer mRID via a mapping table which maps one or more Meter ID values,
                (which for Swtzerland are CH#####) to a CIM mRID.
                Records are summed over multiple meters at one mRID,
                and stored as one Cassandra row per 15 minute reading per EnergyConsumer.
                There is a plan to aggregate these into hourly, three hour, and daily values,
                to be used for quicker client side access when drilling down from overview to specific measurements.
            </aside>
        </section>
        <section data-background="linear-gradient(#c2ccd4, #ffffff)">
            <h3>CIM Generation</h3>
            <img src="img/Generation.svg" style="border-width: 0; background-color: transparent; box-shadow: initial;">
            <audio controls>
                <source src="media/generation.mp3" type="audio/mp3">
            </audio>
            <aside class="notes">
                The GIS system is a very rich source of data;
                every station, transformer, cable, switch, pole, trench, etc.
                both external and internal is maintained in it.
                We use version CIM16. Version CIM17 a.k.a. CIM100 is also possible.
                Full network generation is performed at a once per month cadence.
                Any topology or value changes in the GIS during the month are ignored –
                this may lead to slight inconsistencies with the meter data that is constantly being built out.
                The process merges equipment libraries to attach electrical parameters for cables and transformers.
                Electrical parameters for these standard components are not stored in the GIS –
                there is only a foreign key reference used to join to the equipment libraries.
                A customer specific XML template is used to accommodate schema differences between customers.
                To speed up the process it is performed in parallel across many GIS instances in east-west “stripes”.
                One instance does the northernmost stripe, another the second most northern stripe, and so on.
                Elements that cross stripe boundaries are emitted in multiple stripes,
                so this requires “de-duplication” (based on mRID) when the stripes are read into Spark.
                The process generates CIM files totaling approximately 8-10GB per snapshot.
                When assets are included, as for another use-case, the files are even bigger.
                The files are zipped to conserve space with about a 95% compression ratio,
                resulting in approximately 400MB sets that are stored in per-customer Amazon S3 buckets.
            </aside>
        </section>
        <section data-background="linear-gradient(#c2ccd4, #ffffff)">
            <h3>CIMReader</h3>
            <img src="img/CIMReader.svg" style="border-width: 0; background-color: transparent; box-shadow: initial;">
            <audio controls>
                <source src="media/cimreader.mp3" type="audio/mp3">
            </audio>
            <aside class="notes">
                We use the open source CIMReader, written in Scala, to read the CIM files into Spark.
                Normally we use HDFS to supply all uncompressed stripes.
                Reading and de-duplication takes about five minutes.
                The resultant data structures are the hierarchical CIM class instances in Spark native
                resiliant distributed datasets (RDD) and Spark SQL tables.
                These are in partitions within and across all the Spark workers.
            </aside>
        </section>
        <section data-background="linear-gradient(#c2ccd4, #ffffff)">
            <h3>Topological Analysis</h3>
            <img src="img/Topology.svg" style="border-width: 0; background-color: transparent; box-shadow: initial;">
            <audio controls>
                <source src="media/topology.mp3" type="audio/mp3">
            </audio>
            <aside class="notes">
                A further step in the CIMReader flow, converts the node/breaker model to a bus/branch model
                using topological analysis.
                It uses the Pregel algorithm of the GraphX Spark package and takes about 15 minutes.
                This adds TopologicalNode and TopologicalIsland RDDs and rewrites the Terminal RDD.
                There are various options to force-keep Switches and Fuses
                (which is just like having the CIM retain attribute = true for all elements).
                All 0Ω (internal) cables collapse to one TopologicalNode - a bus.
            </aside>
        </section>
        <section data-background="linear-gradient(#c2ccd4, #ffffff)">
            <h3>CIM Export</h3>
            <img src="img/Export.svg" style="border-width: 0; background-color: transparent; box-shadow: initial;">
            <audio controls>
                <source src="media/export.mp3" type="audio/mp3">
            </audio>
            <aside class="notes">
                The topological analysis identifies topolological islands associated with the low voltage winding
                of each distribution transformer - a transformer service area (or Trafokreis auf Deutsch).
                In this optional step, each transformer service area is exported.
                Exports include all related objects – for example BaseVoltage, WireInfo, Location, and PositionPoints.
                The export generates files on HDFS or records in Cassandra as zipped XML blobs.
                These are used in the interactive use-cases I'll describe later.
                This is also very handy for debugging because it reduces a very large problem
                to something that can be analysed on a laptop.
            </aside>
        </section>
        <section data-background="linear-gradient(#c2ccd4, #ffffff)">
            <h3>Baseline Simulation</h3>
            <img src="img/Simulation.svg" style="border-width: 0; background-color: transparent; box-shadow: initial;">
            <audio controls>
                <source src="media/simulation.mp3" type="audio/mp3">
            </audio>
            <aside class="notes">
                Finally we get to the simulation phase.
                Basically a GridLAB-D model file (a .glm file) is generated for each topological island.
                The program merges parallel “reinforcing” cables, and ganged transformers.
                A SWING node is added to the transformer primary.
                Player files (which can be considered initial conditions at each 15 minute interval)
                are created from the Cassandra smart meter records.
                GridLAB-D is invoked to perform load-flow analysis with the given .glm file,
                reading in the player files and producing recorder files as output.
                Any power factor (cosφ) is inherent in the complex player and recorder values.
                The program also stashes GeoJSON data for nodes, edges and polgons in Cassandra
                for later use as geometric objects by the web-client software to display the results.
            </aside>
        </section>
        <section data-background="linear-gradient(#c2ccd4, #ffffff)">
            <h3>Generate Players</h3>
            <div style="text-align: left;">
                <div style="font-size: smaller">• identify loads using Spark query for EnergyConsumer</div>
                <div style="font-size: smaller">• query Cassandra to create “player” files  (CSV) for loads from smart meter data</div>
                <div style="font-size: smaller">• conversion from energy ⇒ power (×4)</div>
                <div style="font-size: smaller">• back shift one 15 minute period</div>
                <div style="font-size: smaller">• output time stamp and complex value at 15 minute intervals</div>
                <div style="font-size: smaller">• time-series duration based on CIM cadence</div>
            </div>
            <audio controls>
                <source src="media/players.mp3" type="audio/mp3">
            </audio>
            <aside class="notes">
                To generate player files, we first query the CIM data using Spark SQL to find
                the mRID of EnergyConsumers and the topological node they are associated with.
                Then we query Cassandra by mRID for each consumer's meter data,
                to create “player” files in CSV format for each customer load.
                The readings are converted from energy to power
                (basically multiplying by four to go from Watt-Hours per fifteen minutes to Watts),
                They are also back shifted in time, because the meter reading is taken at the end of the 15 minute period,
                and we require the value at the beginning of the period.
                The span of the player time-series corresponds to the cadence of CIM file generation - one month.
            </aside>
        </section>
        <section data-background="linear-gradient(#c2ccd4, #ffffff)">
            <h3>Player Query</h3>
            <div style="text-align: left;">
                <div style="font-family: monospace !important; font-size: 12pt">
<pre style="padding: 0.5em">
<div style="font-size: 14pt">Spark SQL Query</div>
<span style="color: blue">select</span>
    c.ConductingEquipment.Equipment.PowerSystemResource.IdentifiedObject.mRID mrid,
    <span style="color: green">'energy'</span> type,
    concat(c.ConductingEquipment.Equipment.PowerSystemResource.IdentifiedObject.mRID, <span style="color: green">'_load'</span>) name,
    t.TopologicalNode parent,
    <span style="color: green">'constant_power'</span> property,
    <span style="color: green">'Watt'</span> unit,
    n.TopologicalIsland island
<span style="color: blue">from</span>
    EnergyConsumer c,
    Terminal t,
    TopologicalNode n
<span style="color: blue">where</span>
    c.ConductingEquipment.Equipment.PowerSystemResource.PSRType == <span style="color: green">'PSRType_HouseService'</span> and
    c.ConductingEquipment.Equipment.PowerSystemResource.IdentifiedObject.mRID = t.ConductingEquipment and
    t.TopologicalNode = n.IdentifiedObject.mRID
</pre>
                </div>
            </div>
            <div style="text-align: left;">
                <div style="font-family: monospace !important; font-size: 12pt">
<pre style="padding: 0.5em">
<div style="font-size: 14pt">Query Result Fragment</div>
<span style="color: blue">mrid   type   name        parent          property       unit island</span>
HAS160 energy HAS160_load HAS160_topo     constant_power Watt TRA173_terminal_2_island
HAS166 energy HAS166_load HAS166_topo     constant_power Watt TRA161_TRA162_terminal_2_island
HAS23  energy HAS23_load  HAS23_topo      constant_power Watt TRA161_TRA162_terminal_2_island
HAS49  energy HAS49_load  HAS49_fuse_topo constant_power Watt TRA153_terminal_2_island
…
</pre>
                </div>
            </div>
            <div style="text-align: left;">
                <div style="font-family: monospace !important; font-size: 12pt">
<pre style="padding: 0.5em">
<div style="font-size: 14pt">GridLAB-D .glm Fragment</div>
…
object load {
    name "HAS160_load_object";
    parent "HAS160_topo";
    phases AN;
    nominal_voltage 400.0V; };
object player {
    name "HAS160_load";
    parent "HAS160_load_object";
    property "constant_power_A";
    file "input_data/HAS160_load.csv"; };
…
</pre>
                </div>
            </div>
            <audio controls>
                <source src="media/playerquery.mp3" type="audio/mp3">
            </audio>
            <aside class="notes">
                Here is an example Spark SQL query to create players that are loads for EnergyConsumers with a specific PSRType of house service.
                The query results are then translated into .glm load objects almost one-to-one.
                The Cassandra query for each load is written to the player CSV file with the corresponding name.
            </aside>
        </section>
        <section data-background="linear-gradient(#c2ccd4, #ffffff)">
            <h3>Recorders</h3>
            <div style="text-align: left;">
                <div style="font-size: smaller">• Spark queries for “recorder” files for:</div>
                <div style="font-size: 12pt; padding-left: 2em; inset: 1em">· PowerTransformer (N7) apparent power (VA)</div>
                <div style="font-size: 12pt; padding-left: 2em; inset: 1em">· PowerTransformer (N7) current (A)</div>
                <div style="font-size: 12pt; padding-left: 2em; inset: 1em">· BusbarSection (container PSRType_DistributionBox) voltage (V)</div>
                <div style="font-size: 12pt; padding-left: 2em; inset: 1em">· BusbarSection (container PSRType_DistributionBox) apparent power (VA)</div>
                <div style="font-size: 12pt; padding-left: 2em; inset: 1em">· EnergyConsumer (house service) voltage (V)</div>
                <div style="font-size: 12pt; padding-left: 2em; inset: 1em">· EnergyConsumer (house service) apparent power (VA)</div>
                <div style="font-size: 12pt; padding-left: 2em; inset: 1em">· ACLineSegment (N7) current (A)</div>
                <div style="font-size: smaller">• after simulation, read recorders, insert into Cassandra</div>
                <div style="font-size: smaller">• aggregate over 1, 3 and 24 hours</div>
                <div style="font-size: smaller">• time-to-live, 15 minute values “live” a year, daily values forever</div>
            </div>
            <audio controls>
                <source src="media/recorders.mp3" type="audio/mp3">
            </audio>
            <aside class="notes">
                Spark queries are also used to add “recorder” file specifications for:
                    apparent power and current of PowerTransformers
                    busbar apparent power and voltages
                    energy consumer apparent power and voltages
                    and cable currents
                GridLAB-D generates a CSV format file for each output recorder.
                All CSV files are read into Spark, with 15 minute interval values.
                The records are tagged with the mRID and units based on the CSV filename.
                Aggregations are performed over 1, 3 and 24 hours.
                The records are inserted in bulk into Cassandra.
                Disk size is proportional to number of elements and duration of the simulation.
                An average size transformer service area for one month is about 500 million records.
                When stored in Cassandra it consumes approximately 30MB of disk space.
                Currently the bottleneck is insertions per second per Cassandra node.
                Cassandra's “time-to-live” feature is used to age-out the detailed results over time,
                leaving only the aggregate values.
            </aside>
        </section>
        <section data-background="linear-gradient(#c2ccd4, #ffffff)">
            <h3>Recorder Query</h3>
            <div style="text-align: left;">
                <div style="font-family: monospace !important; font-size: 12pt">
<pre style="padding: 0.5em">
<div style="font-size: 14pt">Spark SQL Query</div>
<span style="color: blue">select</span>
    concat (p.ConductingEquipment.Equipment.PowerSystemResource.IdentifiedObject.mRID, <span style="color: green">'_power_recorder'</span>) name,
    p.ConductingEquipment.Equipment.PowerSystemResource.IdentifiedObject.mRID mrid,
    p.ConductingEquipment.Equipment.PowerSystemResource.IdentifiedObject.mRID parent,
    <span style="color: green">'power'</span> type,
    <span style="color: green">'power_out'</span> property,
    <span style="color: green">'VA'</span> unit,
    n.TopologicalIsland island
<span style="color: blue">from</span>
    PowerTransformer p,
    Terminal t,
    TopologicalNode n
<span style="color: blue">where</span>
    t.ConductingEquipment = p.ConductingEquipment.Equipment.PowerSystemResource.IdentifiedObject.mRID and
    t.ACDCTerminal.sequenceNumber > 1 and
    t.TopologicalNode = n.IdentifiedObject.mRID
</pre>
                </div>
            </div>
            <div style="text-align: left;">
                <div style="font-family: monospace !important; font-size: 12pt">
<pre style="padding: 0.5em">
<div style="font-size: 14pt">Query Result Fragment</div>
<span style="color: blue">name                  mrid   parent        type  property  unit island</span>
TRA168_power_recorder TRA168 TRA168_TRA169 power power_out VA   TRA168_TRA169_terminal_2_island
TRA180_power_recorder TRA180 TRA180        power power_out VA   TRA180_terminal_2_island
…
</pre>
                </div>
            </div>
            <div style="text-align: left;">
                <div style="font-family: monospace !important; font-size: 12pt">
<pre style="padding: 0.5em">
<div style="font-size: 14pt">GridLAB-D .glm Fragment</div>
…
object transformer {
    name "TRA168_TRA169";
    phases AN;
    from "MUI4786_topo";
    to "PIN2448_topo";
    configuration "_1260kVA16000$400V590e-5+396e-4jΩ"; };
object recorder {
    name "TRA168_power_recorder";
    parent "TRA168_TRA169";
    property "power_out_A";
    interval "900";
    file "output_data/TRA168_power_recorder.csv"; };
…
</pre>
                </div>
            </div>
            <audio controls>
                <source src="media/recorderquery.mp3" type="audio/mp3">
            </audio>
            <aside class="notes">
                Here is an example Spark SQL query to create transformer power recorders.
                Other recorders are similar.
                This is actually simplified a bit, the full query must join ganged transformers.
                Again the output is almost one-to-one with the .gml fragments.
            </aside>
        </section>
        <section data-background="linear-gradient(#c2ccd4, #ffffff)">
            <h3>Postprocessing</h3>
            <div style="text-align: left;">
                <div style="font-size: smaller">• events are business rules for “problem” conditions</div>
                <div style="font-size: smaller">• include a severity for color coding at the client</div>
                <div style="font-size: 12pt; padding-left: 2em; inset: 1em">· node voltage deviations ±6% at any time, severity 1</div>
                <div style="font-size: 12pt; padding-left: 2em; inset: 1em">· node voltage deviations ±10% at any time, severity 2</div>
                <div style="font-size: 12pt; padding-left: 2em; inset: 1em">· edge current exceeding 75% for 14 consecutive hours, severity 1</div>
                <div style="font-size: 12pt; padding-left: 2em; inset: 1em">· edge current exceeding 90% for 3 consecutive hours, severity 2</div>
                <div style="font-size: 12pt; padding-left: 2em; inset: 1em">· edge current exceeding 110% at any time, severity 2</div>
                <div style="font-size: 12pt; padding-left: 2em; inset: 1em">· transformer power exceeding 75% for 14 consecutive hours, severity 1</div>
                <div style="font-size: 12pt; padding-left: 2em; inset: 1em">· transformer power exceeding 90% for 3 consecutive hours, severity 2</div>
                <div style="font-size: 12pt; padding-left: 2em; inset: 1em">· transformer power exceeding 110% at any time, severity 2</div>
                <div style="font-size: smaller">• save events in Cassandra tagged by transformer service area</div>
                <div style="font-size: smaller">• coincidence/diversity factor, load factor, responsibility factor</div>
            </div>
            <audio controls>
                <source src="media/postprocessing.mp3" type="audio/mp3">
            </audio>
            <aside class="notes">
                After simulation, there is a postprocessing phase to identify events and other metrics.
                Events are business rules for “problem” conditions.
                They include a severity, for color coding at the client.
                Event triggers currently include:
                - node voltage deviations
                - edge current excesses
                - and transformer power excesses
                Events are saved in Cassandra tagged by transformer service area.
                Other germane metrics such as coincidence, load and responsibilty factors are also computed.
            </aside>
        </section>
        <section data-background="linear-gradient(#c2ccd4, #ffffff)">
            <h3>Dashboard</h3>
            <img src="img/Dashboard2.png" style="border-width: 0; background-color: transparent; box-shadow: initial;">
            <audio controls>
                <source src="media/dashboard.mp3" type="audio/mp3">
            </audio>
            <aside class="notes">
                Results are displayed via a dashboard.
                The dashboard consists of an initial overview map with transformer service areas as convex hull polygons,
                from the GeoJSON stored earlier.
                The polygons are colored/edged/patterned by the summary of event types and severity.
                Selecting a polygon draws the nodes and edges, colored by event severity (red, amber, green).
                Selecting a node or edge displays a chart of raw data and/or simulated time-series.
            </aside>
        </section>
        <section data-background="linear-gradient(#c2ccd4, #ffffff)">
            <h3>Use-case: N-1</h3>
            <img src="img/NMinus1.svg" style="border-width: 0; background-color: transparent; box-shadow: initial;">
            <audio controls>
                <source src="media/nminus1.mp3" type="audio/mp3">
            </audio>
            <aside class="notes">
                The N-1 redundancy use-case is basically
                "Can neighbouring service areas support a problem area?"
                One starts by assembling the problem in the browser,
                pulling in transformer service areas from CIM zip blobs stored earlier, from the desired snapshot.
                The user then adjusts some switch normalOpen/open states and
                saves it as a project (a “what-if” scenario).
                A simulation with the new topology is performed and saved in the “project” keyspace in Cassandra.
                The client software displays a map-based drill-down analysis dashboard of the project.
                In this and the following use-case, the baseline simulation can be superimposed in these time-series charts.
                The intent is to automate this analysis over the whole distribution grid eventually.
            </aside>
        </section>
        <section data-background="linear-gradient(#c2ccd4, #ffffff)">
            <h3>Use-case: Future DER</h3>
            <img src="img/FutureDER.svg" style="border-width: 0; background-color: transparent; box-shadow: initial;">
            <audio controls>
                <source src="media/futureder.mp3" type="audio/mp3">
            </audio>
            <aside class="notes">
                The future distributed energy resource use-case is basically
                "What happens when local generation and batteries become significant?"
                Again, one starts by assembling the problem in the browser,
                pulling in a transformer service area,
                and then the user attaches solar generation and/or battery storage to some energy consumer(s) and
                saves it as a project.
                The user then attaches synthetic profile data to the new equipment.
                These synthetic profiles are at the front-line coal face of current development.
                A simulation with the added elements is performed and saved in the "project" keyspace in Cassandra.
                The client software displays a map-based drill-down analysis dashboard as before.
                The intent is to simulate adding DER to all statistically likely locations over
                the whole distribution grid eventually.
            </aside>
        </section>
        <section data-background="linear-gradient(#c2ccd4, #ffffff)">
            <h3>Use-case: New Consumer</h3>
            <img src="img/NewConsumer.svg" style="border-width: 0; background-color: transparent; box-shadow: initial;">
            <audio controls>
                <source src="media/newconsumer.mp3" type="audio/mp3">
            </audio>
            <aside class="notes">
                The new consumer use-case is basically
                "Can the network support a new consumer? If so, from which distribution box and what are the new element dimensions?"
                Again, one starts by assembling the problem in the browser,
                pulling in a transformer service area,
                and then the user adds new cables and energy consumer(s) and
                saves it as a project.
                The user then attaches synthetic profile data, or alternatively,
                links data from a similar existing smart meter to the new consumers.
                A simulation with the added elements is performed and saved in the "project" keyspace in Cassandra.
                The client software displays a map-based drill-down analysis dashboard as before.
                The intent is to provide dimensioning and connection help to planning engineers.
            </aside>
        </section>
        <section data-background="linear-gradient(#c2ccd4, #ffffff)">
            <h3>Summary</h3>
            <div style="text-align: left;">
                <div style="font-size: smaller">• provides distribution level simulation with:</div>
                <div style="font-size: 12pt; padding-left: 2em; inset: 1em">· Apache Spark</div>
                <div style="font-size: 12pt; padding-left: 2em; inset: 1em">· Apache Cassandra</div>
                <div style="font-size: 12pt; padding-left: 2em; inset: 1em">· CIM</div>
                <div style="font-size: 12pt; padding-left: 2em; inset: 1em">· smart meter data</div>
                <div style="font-size: smaller">• uses topological partitioning</div>
                <div style="font-size: smaller">• performs baseline load-flow simulation</div>
                <div style="font-size: smaller">• allows project/use-cases:</div>
                <div style="font-size: 12pt; padding-left: 2em; inset: 1em">· N-1</div>
                <div style="font-size: 12pt; padding-left: 2em; inset: 1em">· new distributed energy resources</div>
                <div style="font-size: 12pt; padding-left: 2em; inset: 1em">· new consumer</div>
            </div>
            <audio controls>
                <source src="media/summary.mp3" type="audio/mp3">
            </audio>
            <aside class="notes">
                In summary, simulating the low voltage network is possible given:
                - suitable computation and database resources
                - CIM data
                - and smart meter data
                The trick is to topologically partition the problem into transformer service areas.
                The baseline load-flow is interesting in and of itself, but
                various use-cases make a compelling service for distribution engineers.
                Thank you for your attention.
            </aside>
        </section>
    </div>
</div>

<script src="js/reveal.js"></script>

<script>
    // More info about config & dependencies:
    // - https://github.com/hakimel/reveal.js#configuration
    // - https://github.com/hakimel/reveal.js#dependencies
    Reveal.initialize({
        hash: true,
        history: true,
        dependencies: [
            { src: 'plugin/markdown/marked.js' },
            { src: 'plugin/markdown/markdown.js' },
            { src: 'plugin/notes/notes.js', async: true },
            { src: 'plugin/highlight/highlight.js', async: true }
        ]
    });
</script>
</body>
</html>
